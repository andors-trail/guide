# frozen_string_literal: true

require_relative 'andors_trail_guide/version'

module AndorsTrailGuide
  class Error < StandardError; end
  # Your code goes here...
end
